# Set up gems listed in the Gemfile.
ENV['BUNDLE_GEMFILE'] ||= File.expand_path('../../Gemfile', __FILE__)

require 'bundler/setup' if File.exist?(ENV['BUNDLE_GEMFILE'])

# Here are two ways to add Node.js as the JavaScript runtime for a rails app.
ENV['EXECJS_RUNTIME'] = 'Node'
